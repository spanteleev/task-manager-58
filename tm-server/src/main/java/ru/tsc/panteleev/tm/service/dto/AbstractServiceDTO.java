package ru.tsc.panteleev.tm.service.dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.repository.dto.IRepositoryDTO;
import ru.tsc.panteleev.tm.api.service.dto.IServiceDTO;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;

@Service
public abstract class AbstractServiceDTO<M extends AbstractModelDTO, R extends IRepositoryDTO<M>> implements IServiceDTO<M> {

    @Autowired
    protected ApplicationContext context;

    protected abstract IRepositoryDTO<M> getRepository();

}
