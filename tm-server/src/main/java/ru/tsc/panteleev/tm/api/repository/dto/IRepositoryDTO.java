package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;

public interface IRepositoryDTO<M extends AbstractModelDTO> {

    @NotNull
    EntityManager getEntityManager();

    void add(@NotNull M model);

    void update(@NotNull final M model);

    void remove(@NotNull final M model);

}
