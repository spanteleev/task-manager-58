package ru.tsc.panteleev.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.model.AbstractModel;

import javax.persistence.EntityManager;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    EntityManager getEntityManager();

    void add(@NotNull M model);

    void update(@NotNull final M model);

    void remove(@NotNull final M model);

}
